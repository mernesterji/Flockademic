import { getSlugForName } from '../../filters/periodical';

describe('getSlugForName', () => {
  it('should remove special characters', () => {
    expect(getSlugForName('journal of ♥')).toBe('journal-of-love');
  });

  it('should lowercase names', () => {
    expect(getSlugForName('Journal Of Uppercase')).toBe('journal-of-uppercase');
});

  it('should trim trailing dashes', () => {
    expect(getSlugForName('journal of trailing dashes --')).toBe('journal-of-trailing-dashes');
  });

  it('should trim anything beyond 35 characters', () => {
    expect(getSlugForName('journal of far more than 35 characters')).toBe('journal-of-far-more-than-35-charact');
  });

  it('should trim trailing dashes remaining at character 35', () => {
    expect(getSlugForName('journal of so appropriately placed spaces')).toBe('journal-of-so-appropriately-placed');
  });
});
